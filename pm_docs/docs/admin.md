#Admin


## Admin Users

`Request` to list all admin users

```json
GET /admin/users?type=admin
```


## API Users

`Request` to list all api users

```json
GET /users

```

## Client

```
GET /clients
```

`Response Body`

```json
{
	"data": [],
	"paging":{
		"next": {},
		""
	},
	"status": {
		"msg": ""
		"code": "",
		"success": "true"
	}
}
```


